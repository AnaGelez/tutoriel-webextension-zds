chrome.runtime.onMessage.addListener(function (requete, envoyeur, repondre){
    document.getElementById('temps').innerHTML = beauTemps(requete.temps);
});

function beauTemps (temps) {
    var heures = Math.floor(temps / 3600);
    var minutes = Math.floor((temps / 60) % 60);
    var secondes = temps % 60;
    return `${heures}:${minutes}:${secondes}`;
}

document.getElementById('parametres').addEventListener('click', function () {
    var tempsActuel = 3600 || chrome.storage.local.get('temps', function(){});
    var nouveauTemps = prompt('Entrez le nombre de secondes que vous pouvez passer sur Internet :', new String(tempsActuel));
    chrome.strorage.local.set({temps: nouveauTemps}, function(){});
});
