// Par défaut on a un temps de 1h.
var tempsRestant = 3600;

// Si l’utilisateur a choisi autre chose, on prends cette valeur.
var tempsUtilisateur = chrome.storage.local.get('temps', function(){});

if (tempsUtilisateur) tempsRestant = tempsUtilisateur;

chrome.alarms.create ('chrono', {
    delayInMinutes : 0.0,
    periodInMinutes : 1 / 60
});

chrome.alarms.onAlarm.addListener (function() {
    if (tempsRestant == 0) {
        // On obtient toutes les fenêtres.
        chrome.windows.getAll(function(fenetres) {
            // On les passe une à une.
            fenetres.forEach(function(fenetre) {
                // Et on les ferme.
                chrome.windows.remove(fenetre.id);
            });
        });
    } else if (tempsRestant == 5 * 60) {
        chrome.notifications.create({
            type: "basic",
            iconUrl: 'ico512.png',
            title: 'Enregistrez votre travail',
            message: 'Il ne vous reste que 5 minutes de surf, dépêchez-vous !'
        });
    }

    chrome.runtime.sendMessage({temps : tempsRestant});
    tempsRestant--;
});